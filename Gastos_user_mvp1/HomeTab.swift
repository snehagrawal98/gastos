//
//  HomeTab.swift
//  Gastos_user_mvp1
//
//  Created by ehsan sat on 10/13/21.
//

import SwiftUI

struct HomeTab: View {
    
    private var columns: [GridItem] = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        
        NavigationView(content: {
            
        VStack(content: {
            HStack {
                Text("Hey Tarun!")
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                Spacer()
                
                NavigationLink(
                    destination: UserProfile(),
                    label: {
                        Image(systemName: "bell.badge")
                            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                            .foregroundColor(.green)

                    })
                    .navigationTitle("")
                    .navigationBarHidden(true)
                    .navigationBarBackButtonHidden(true)
            }
            .padding(.horizontal)
            
            NavigationLink(
                destination: UserInformationScreen(),
                label: {
                    RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                        .frame(width: UIScreen.main.bounds.width - 64, height: Constants.sH * 0.3, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.green)

                })
                .navigationTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
            
            LazyVGrid(columns: columns, content: {
                
                ForEach(0..<7) { i in
                    
                    NavigationLink(
                        destination: Places(),
                        label: {
                            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                                
                                
                                RoundedRectangle(cornerRadius: 10)
                                    .frame(width: Constants.sH * 0.08, height: Constants.sH * 0.08, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                Text("\(HomeCategories.allCases[i].rawValue)")
                                    .font(.caption)
                                    .lineLimit(1)
                            })

                        })
                        .navigationBarHidden(true)
                        .navigationTitle("")
                        .navigationBarBackButtonHidden(true)
                        .navigationBarTitle("")
                }
            })
            .padding(.horizontal)
            .padding(.top, 6)
            
            ScrollView(.horizontal, showsIndicators: false, content: {
                HStack {
                    ForEach(0..<10) { i in
                        Rectangle()
                            .fill(LinearGradient(gradient: Gradient(colors: [Color.blue, Color.green]), startPoint: /*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/, endPoint: /*@START_MENU_TOKEN@*/.trailing/*@END_MENU_TOKEN@*/))
                            .frame(width: 230, height: Constants.sH * 0.2, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .cornerRadius(10)
                            .shadow(color: .black, radius: 5, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
                    }
                    .padding()
                }
            })
            .frame( height: Constants.sH * 0.2)
            .padding(.leading)

            })
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        })
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

enum HomeCategories: String, CaseIterable {
    case food = "Food"
    case beverage = "Beverage"
    case clothing = "Clothing"
    case salon = "Salon"
    case stores = "Stores"
    case health = "Health"
    case others = "Others"
}

struct HomeTab_Previews: PreviewProvider {
    static var previews: some View {
        HomeTab()
    }
}
