//
//  EnterMobileNumber.swift
//  Gastos_user_mvp1
//
//  Created by mnameit on 17/10/21.
//

import SwiftUI


struct EnterMobileNumber: View {
    var body: some View {
        
        ZStack{
            
            Image("Layer").offset(x: 100.0, y: -250.0)
        VStack{
            
            HStack{
                Text("Welcome to").fontWeight(.semibold).font(.system(size: 25)).foregroundColor(Color("5")).shadow(radius: 3, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 5).padding(.horizontal)
                Spacer()
            }
            
            HStack{
                Text("Gastos").fontWeight(.bold).font(.system(size: 40)).foregroundColor(Color("5")).shadow(radius: 3, x: 5, y: 5).padding(.horizontal)
                Spacer()
            }
            
            Spacer()
            HStack{
                Text("Enter Mobile Number").fontWeight(.regular).font(.system(size: 25)).foregroundColor(Color("5")).padding(.horizontal)
                Spacer()
            }
            
          //  TextField("")
            Spacer()
        }.padding(.vertical, 50)
        }
    }
}

struct EnterMobileNumber_Previews: PreviewProvider {
    static var previews: some View {
        EnterMobileNumber()
    }
}



extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .bottomLeft

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
