//
//  ProfilePage.swift
//  Gastos_user_mvp1
//
//  Created by mnameit on 18/10/21.
//

import SwiftUI

struct ProfilePage: View {
    
    @State private var firstName = ""
    @State private var lastName = ""
    @State private var emailAddress = ""
    @State private var code = ""
    @State var date = Date()

    
    var body: some View {
        NavigationView{
            VStack{
                Text("Personal Details").foregroundColor(Color("5")).fontWeight(.medium).font(.system(size: 25))
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                   
                        Image(systemName: "person.crop.circle.fill.badge.plus").font(.system(size: 80)).foregroundColor(Color("6"))
                       
                    
                })
                
                HStack(spacing: 0){
                    TextField("First Name", text: $firstName).frame(width: 190).textFieldStyle(MyTextFieldStyle())
                    TextField("Last Name", text: $lastName).frame(width: 190).textFieldStyle(MyTextFieldStyle())
                }
                
                TextField("E-Mail address", text: $emailAddress).textFieldStyle(MyTextFieldStyle())
                
                HStack{
                    Text("Birth Date").foregroundColor(Color("5")).font(.system(size: 19))
                    DatePicker("", selection: $date, displayedComponents: .date).colorInvert()
                        .colorMultiply(Color.blue)
                }.padding(.horizontal)
                
                HStack{
                    Text("Gender").foregroundColor(Color("5")).font(.system(size: 19))
                    Spacer()
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        HStack{
                        Image("female")
                            Text("female")
                        }
                    }).padding().background(Color.white).background(RoundedCorner().cornerRadius(10.0).shadow(color: Color("gray"), radius: 8, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 0.0))
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        HStack{
                        Image("male")
                            Text("male")
                        }
                    }).padding().background(Color.white).background(RoundedCorner().cornerRadius(10.0).shadow(color: Color("gray"), radius: 8.0, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 0.0))
                }.padding(.horizontal).padding(.vertical, 5)
                
                HStack{
                    Text("Have a referral code?").foregroundColor(Color("5")).font(.system(size: 19)).padding(.leading)
                Spacer()
                }
                ZStack{
                TextField("Enter Code", text: $emailAddress).textFieldStyle(MyTextFieldStyle())
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Validate").foregroundColor(.white)
                    }).padding(5).background(Color("7")).cornerRadius(5.0).offset(x: 120)
                    
                }
                
                
                HStack{
                    Spacer()
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Image(systemName: "chevron.right").font(.system(size: 20)).foregroundColor(.white).frame(width: 45, height: 45, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    }).padding(3).background(Color("7")).clipShape(Circle()).padding()
                }
                
            }.navigationBarItems(leading: Image(systemName: "arrow.backward")).foregroundColor(Color("5"))
        }
    }
}

struct ProfilePage_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePage()
    }
}
